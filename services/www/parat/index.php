<?php

date_default_timezone_set("Asia/Bangkok");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Max-Age: 60");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

$json = file_get_contents('php://input');

$data = json_decode($json);

echo json_encode($data);
?>