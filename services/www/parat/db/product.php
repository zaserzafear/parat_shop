<?php

require_once 'db.php';

class products {
    
    private $selectField = "p_id, p_code, p_img, p_price";

    public function getAllProduct() {
        $sql = "SELECT $this->selectField FROM tb_product WHERE isDeleted = '0'";
        $param = [];

        $db = new database();
        $query = $db->execute($sql, $param);
        $record = $query->fetchAll(PDO::FETCH_ASSOC);
        return $record;
    }

    public function getProductByCode($code) {
        $sql = "SELECT $this->selectField FROM tb_product WHERE p_code = :p_code";
        $param = [":p_code" => "$code"];

        $db = new database();
        $query = $db->execute($sql, $param);
        $record = $query->fetchAll(PDO::FETCH_ASSOC);
        return $record;
    }

    public function getProductById($id) {
        $sql = "SELECT $this->selectField FROM tb_product WHERE p_id = :p_id";
        $param = [":p_id" => "$id"];

        $db = new database();
        $query = $db->execute($sql, $param);
        $record = $query->fetchAll(PDO::FETCH_ASSOC);
        return $record;
    }

    public function insertProduct($param) {
        $p_code = $param["p_code"];
        $checkCode = $this->getProductByCode($p_code);
        if (count($checkCode) > 0) {
            http_response_code(400);
            return ["status" => 400, "message" => "Product Code $p_code Has Already In Database."];
        }
        
        $sql = "INSERT tb_product (p_code, p_name, p_img, p_price) "
                . "VALUES (:p_code, :p_name, :p_img, :p_price)";
        $param = [":p_code" => $p_code, ":p_name" => $param["p_name"], ":p_img" => "", ":p_price" => $param["p_price"]];

        $db = new database();
        $db->execute($sql, $param);
        $id = $db->getLastInsertId();
        return $this->getProductById($id);
    }

    public function updateProduct($param) {
        $p_id = $param['p_id'];
        $checkId = $this->getProductById($p_id);
        if (count($checkId) == 0) {
            http_response_code(404);
            return ["status" => 404, "message" => "Product Id $p_id Not Found In Database."];
        }

        $sql = "UPDATE tb_product "
                . "SET p_name = :p_name"
                . ", p_img = :p_img"
                . ", p_price = :p_price "
                . "WHERE p_id = :p_id";
        $param = [":p_name" => $param["p_name"], ":p_img" => "", ":p_price" => $param["p_price"], ":p_id" => $p_id];

        $db = new database();
        $db->execute($sql, $param);
        return $this->getProductByid($id);
    }
    
    public function deleteProduct($param) {
        $p_id = $param['p_id'];
        $checkId = $this->getProductById($p_id);
        if (count($checkId) == 0) {
            http_response_code(404);
            return ["status" => 404, "message" => "Product Id $p_id Not Found In Database."];
        }

        $sql = "UPDATE tb_product "
                . "SET isDeleted = '1' "
                . "WHERE p_id = :p_id";
        $param = [":p_id" => $p_id];

        $db = new database();
        $db->execute($sql, $param);
        return $this->getProductByid($id);
    }

}

?>