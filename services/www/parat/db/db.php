<?php

class database {

    private $link = null;
    private $lastInsertId = 0;

    public function __construct() {
        $this->link = new PDO('mysql:host=db;dbname=parat_shop_db;charset=utf8mb4', 'parat', 'xhkiy9oN', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_PERSISTENT => false));
    }

    public function __destruct() {
        $this->link = null;
    }

    public function execute($sql, $params) {
        try {
            $stmt = $this->link->prepare($sql);
            $stmt->execute($params);
            $this->lastInsertId = $this->link->lastInsertId();            
            return $stmt;
        } catch (PDOException $ex) {
            echo "<pre>", var_dump($ex->getMessage()), "</pre>";
        }
    }
    
    public function getLastInsertId() {
        return $this->lastInsertId;
    }

}

?>
