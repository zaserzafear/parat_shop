<?php

require_once 'db/product.php';
$product = new products();

header('Content-Type: application/json');
$data = json_decode(file_get_contents('php://input'), true);
$method = $_SERVER['REQUEST_METHOD'];
$p_id = $data['p_id'];
$p_code = $data['p_code'];
$p_name = $data['p_name'];
$p_price = $data['p_price'];
$param = ["p_id" => $p_id, "p_code" => "$p_code", "p_name" => "$p_name", "p_price" => "$p_price"];

switch ($method) {
    case "GET":
        $p_code = $_GET["code"];
        $result = ($p_code == '') ? $product->getAllProduct() : $product->getProductByCode($p_code);
        echo json_encode($result, JSON_NUMERIC_CHECK);
        break;
    case "POST":
        $result = $product->insertProduct($param);
        echo json_encode($result, JSON_NUMERIC_CHECK);
        break;
    case "PUT":
        $result = $product->updateProduct($param);
        echo json_encode($result, JSON_NUMERIC_CHECK);
        break;
    case "DELETE":
        $result = $product->deleteProduct($param);
        echo json_encode($result, JSON_NUMERIC_CHECK);
        break;
}
?>